import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

import 'fetch_file.dart';

class InfoPage extends StatefulWidget {
  final info;
  InfoPage({Key key, this.info}) : super(key: key);

  @override
  _InfoPageState createState() => _InfoPageState();
}

class _InfoPageState extends State<InfoPage> {
  Map artist = {};

  @override
  void initState() {
    fetchFileFromAssets('assets/artists.json').then((String jsonString) {
      final jsonResponse = jsonDecode(jsonString);
      setState(() {
        for (var element in jsonResponse) {
          if (element['link'] == widget.info) {
            artist = element;
          }
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final String title = artist.isEmpty ? 'Loading...' : artist['name'];
    final String aboutBand = artist.isEmpty ? 'Loading...' : artist['about'];

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(aboutBand),
          )
        ],
      ),
    );
  }
}
