import 'dart:convert';

import 'package:albums_route/router.dart';
import 'package:flutter/material.dart';

import 'fetch_file.dart';
import 'menu.dart';

class Artists extends StatefulWidget {
  Artists({Key key}) : super(key: key);

  @override
  _ArtistsState createState() => _ArtistsState();
}

class _ArtistsState extends State<Artists> {
  List artists = [];

  @override
  void initState() {
    fetchFileFromAssets('assets/artists.json').then((String jsonString) {
      final jsonResponse = jsonDecode(jsonString);
      setState(() {
        artists = jsonResponse;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Artists'),
      ),
      drawer: Menu(),
      body: ListView(
        children: [
          Column(
            children: artists.isEmpty
                ? [Text('Loading...')]
                : artists
                    .map((band) => ListTile(
                          title: Text(
                              "${band['name'][0].toUpperCase()}${band['name'].substring(1)}"),
                          onTap: () {
                            MyRouter.router.navigateTo(
                              context,
                              "/albums/${band['link']}",
                              routeSettings: RouteSettings(
                                arguments: band,
                              ),
                            );
                          },
                        ))
                    .toList(),
          )
        ],
      ),
    );
  }
}
