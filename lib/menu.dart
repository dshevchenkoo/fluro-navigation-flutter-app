import 'package:flutter/material.dart';
import 'router.dart';

class Menu extends StatelessWidget {
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Column(children: [
            ListTile(
              title: Text('Home'),
              onTap: () {
                MyRouter.router.navigateTo(
                  context,
                  '/',
                );
              },
            ),
            ListTile(
              title: Text('Artists'),
              onTap: () {
                MyRouter.router.navigateTo(
                  context,
                  '/albums',
                  routeSettings: RouteSettings(),
                );
              },
            ),
          ])
        ],
      ),
    );
  }
}
