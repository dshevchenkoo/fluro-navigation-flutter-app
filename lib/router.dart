import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'info.dart';
import 'main.dart';
import 'artists.dart';

class MyRouter {
  static FluroRouter router = FluroRouter();

  static Handler _homeHandler = Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
          MyHomePage());

  static Handler _menuAlbumsHandler = Handler(
    handlerFunc: (context, params) {
      return Artists();
    },
  );

  static Handler _infoHandler = Handler(
    handlerFunc: (context, params) {
      final artist = params['artist'][0];
      return InfoPage(info: artist);
    },
  );

  static void setupRouter() {
    router.define('/',
        handler: _homeHandler, transitionType: TransitionType.fadeIn);
    router.define('/albums/:artist',
        handler: _infoHandler, transitionType: TransitionType.inFromRight);
    router.define('/albums',
        handler: _menuAlbumsHandler, transitionType: TransitionType.inFromLeft);
  }
}
